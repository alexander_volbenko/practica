package sample;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class DatabaseHandlerTest {
    DatabaseHandler databaseHandler = new DatabaseHandler();

    /**
     * Тест проверяющий соеденинение с бд
     */
    @org.junit.Test
    public void getDbConnection() {
        try {
            assertFalse(databaseHandler.getDbConnection().isClosed());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тест для метода проверки совпадения id указанное пользователем и в базе данных
     */
    @org.junit.Test
    public void updateCheck() {
        assertTrue(databaseHandler.updateCheck("личные данные студента", "1"));
    }
}