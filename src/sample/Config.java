package sample;

/**
 * Класс с переменными для быстрой работы и изменения
 */
public class Config {
    protected String dbHost = "localhost";
    protected String dbPort = "3306";
    protected String dbUser = "root";
    protected String dbPass = "password";
    protected String dbName = "practica";

    //Массив с названиями колонок таблиц базы данных
    protected String[][] columns = {
            {"сведения о студентах", "Код студента", "ФИО", "Датa рождения", "Домашний адрес", "Телефон"},
            {"сведения о родителях", "Код студента", "Статус семьи", "Количество детей", "ФИО м", "Адрес м", "Телефон м", "Место работы м", "ФИО п", "Адрес п", "Телефон п", "Место работы п"},
            {"личные данные студента", "Код студента", "ФИО", "Датa рождения", "Место рождения", "Серия номер", "Дата выдачи", "Кем выдан код отделения", "ИНН", "СНИЛС", "СР серия номер", "СР кем выдан", "СР дата выдачи"},
            {"успеваемость", "Код студента", "Название предмета", "Имя преподавателя", "Оценка"}
    };

    //Массив с названиями переменных объектов для заполнения таблицы
    protected String[][] objectsValues = {
            {"stId", "fio", "dateOfBirth", "address", "telephone"},
            {"stId", "familyStatus", "numberOfChildren", "fiom", "adressm", "telephonem", "jobPlacem", "fiop", "adressp", "telephonep", "jobPlacepp"},
            {"stId", "fio", "dateOfBirthPas", "placeOfBirthPas", "seriesAndNumberPas", "issuesDatePas", "issuedByPasandCodePas", "inn", "snils", "seriesAndNumberBC", "issuedByBC", "issuesDateBC"}
    };
}
