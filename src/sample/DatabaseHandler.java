package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import res.ParentsInfo;
import res.StudentsData;
import res.StudentsInfo;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseHandler extends Config {
    Connection dbConnection;


    /**
     * Метод создающий соеденение с базой данных
     * @return соеденение с базой данных
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?verifyServerCertificate=false&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        Class.forName("com.mysql.cj.jdbc.Driver");
        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);
        return dbConnection;
    }

    /**
     * Метод закрытия соеденения с базой данных
     */
    private void connectionClose() {
        try {
            dbConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод добавления в базу данных или ее редактирование
     * @param tableName - название таблицы
     * @param values - значения для их добавление
     */
    public void insert(String tableName, ArrayList<String> values) {
        StringBuilder dbStr = new StringBuilder("insert into `" + tableName + "`(");
        String[] columns = tableSelect(tableName);
        if (updateCheck(tableName, values.get(0))) {
            update(tableName,values,columns);
        } else {
            if (columns[0].equals(tableName)) {
                for (int i = 1; i < columns.length; i++) {
                    dbStr.append("`" + columns[i] + "`");
                    if (i == columns.length - 1) dbStr.append(")");
                    else dbStr.append(",");
                }
                dbStr.append(" values (");
                for (int i = 0; i < values.size(); i++) {
                    dbStr.append('"' + values.get(i) + '"');
                    if (i == values.size() - 1) dbStr.append(")");
                    else dbStr.append(",");
                }
            }
            try {
                PreparedStatement prSt = getDbConnection().prepareStatement(dbStr.toString());
                prSt.executeUpdate();
                prSt.close();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        connectionClose();
    }

    /**
     * Метод для проверки на редактирование существубщий данных
     * @param tableName - название таблицы
     * @param id - переменная для сравнение с id в базе данных
     * @return - true or false
     */
    public boolean updateCheck(String tableName, String id) {
        boolean checkStatus = false;
        try {
            PreparedStatement prSt = getDbConnection().prepareStatement("select * from `" + tableName + "`");
            ResultSet resultSet = prSt.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(id)) checkStatus = true;
            }
            prSt.close();
            resultSet.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return checkStatus;
    }

    /**
     * Метод для изменения готовых данных в базе данных
     * @param tableName - название таблицы
     * @param values - значение для внесения в базу данных
     * @param columns - массив с колонками таблицы базы данных
     */
    private void update(String tableName, ArrayList<String> values, String[] columns) {
        StringBuilder sqlStr = new StringBuilder("update `" + tableName + "` set");
        for (int i = 2; i < columns.length; i++) {
            sqlStr.append(" `").append(columns[i]).append("` = ").append('"').append(values.get(i-1)).append('"');
            if (i!= columns.length-1) sqlStr.append(",");
        }
        sqlStr.append(" where `").append(columns[1]).append("` = ").append('"').append(values.get(0)).append('"');
        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(sqlStr.toString());
            prSt.executeUpdate();
            prSt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод выбора массива названия колонок
     * @param tableName - название таблицы
     * @return - массив с названиями колонок
     */
    private String[] tableSelect(String tableName) {
        for (String[] column : columns) {
            if (column[0].equals(tableName)) {
                return column;
            }
        }
        return new String[]{"error"};
    }

    /**
     * Метод возвращающий массив с данными базы данных для заполнение таблицы приложения
     * @param tableId - цифра таблицы в массиве значений
     * @return - массив значений базы данных
     */
    ObservableList dbGetInfo(int tableId) {
        ObservableList objectsList = FXCollections.observableArrayList();
        String dbStr = "select * from `" + columns[tableId][0] + "`";
        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(dbStr);
            ResultSet resultSet = prSt.executeQuery();
            while (resultSet.next()) {
                String[] arr = new String[columns[tableId].length - 1];
                for (int i = 0; i < columns[tableId].length - 1; i++) {
                    arr[i] = resultSet.getString(i + 1);
                }
                switch (tableId) {
                    case 0:
                        objectsList.add(new StudentsInfo(arr[0], arr[1], arr[2], arr[3], arr[4]));
                        break;
                    case 1:
                        objectsList.add(new ParentsInfo(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10]));
                        break;
                    case 2:
                        objectsList.add(new StudentsData(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10], arr[11]));
                        break;
                }
            }
            prSt.close();
            connectionClose();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return objectsList;
    }
}
