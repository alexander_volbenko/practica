package sample;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import res.StudentsInfo;

import java.util.ArrayList;

/**
 * Класс управления графическим интерфейсом
 */
public class Controller extends DatabaseHandler {
    public javafx.scene.control.ScrollPane ScrollPane;
    public javafx.scene.control.ChoiceBox<String> ChoiceBox;
    public TextField tf1;
    public TextField tf2;
    public TextField tf3;
    public TextField tf4;
    public TextField tf5;
    public TextField tf6;
    public TextField tf7;
    public TextField tf8;
    public TextField tf9;
    public TextField tf10;
    public TextField tf11;
    public TextField tf12;
    public Button saveButton;
    public Label error;
    private DatabaseHandler db = new DatabaseHandler();
    private Config config = new Config();

    private int tableChooseId = 0;

    /**
     * Метод для добавления и редактирования данных в БД с помощью нажатия кнопки
     */
    @FXML
    private void insert() {
        error.setText("");
        if (tf1.getText().equals(""))
            error.setText("Пустое поле кода студента");
        else {
            ArrayList<String> textFieldValues = new ArrayList<>();
            if (tableChooseId >= 0) {
                textFieldValues.add(tf1.getText());
                textFieldValues.add(tf2.getText());
                textFieldValues.add(tf3.getText());
                textFieldValues.add(tf4.getText());
                textFieldValues.add(tf5.getText());
            }
            if (tableChooseId >= 1) {
                textFieldValues.add(tf6.getText());
                textFieldValues.add(tf7.getText());
                textFieldValues.add(tf8.getText());
                textFieldValues.add(tf9.getText());
                textFieldValues.add(tf10.getText());
                textFieldValues.add(tf11.getText());
            }
            if (tableChooseId == 2) {
                textFieldValues.add(tf12.getText());
            }
            db.insert(columns[tableChooseId][0], textFieldValues);
            ScrollPane.setContent(TableViewCreate(tableChooseId));
        }
    }

    /**
     * Метод выбора таблицы студентов из базы данных для просмотра
     */
    public void dbStudents() {
        ScrollPane.setContent(TableViewCreate(0));
        saveButton.setVisible(true);
        error.setText("");
        studentsTF();
    }

    /**
     * Метод изменения внешности и заднего текста текстовых полей
     */
    private void studentsTF() {
        tf1.setVisible(true);
        tf1.setPromptText(columns[0][1]);
        tf2.setVisible(true);
        tf2.setPromptText(columns[0][2]);
        tf3.setVisible(true);
        tf3.setPromptText(columns[0][3]);
        tf4.setVisible(true);
        tf4.setPromptText(columns[0][4]);
        tf5.setVisible(true);
        tf5.setPromptText(columns[0][5]);
        tf6.setVisible(false);
        tf7.setVisible(false);
        tf8.setVisible(false);
        tf9.setVisible(false);
        tf10.setVisible(false);
        tf11.setVisible(false);
        tf12.setVisible(false);
    }

    /**
     * Метод выбора таблицы родителей их базы данных для просмотра
     * @param actionEvent
     */
    public void dbParents(ActionEvent actionEvent) {
        ScrollPane.setContent(TableViewCreate(1));
        saveButton.setVisible(true);
        error.setText("");
        parentsTF();
    }

    /**
     * Метод изменения внешности и заднего текста текстовых полей
     */
    private void parentsTF() {
        tf1.setVisible(true);
        tf1.setPromptText(columns[1][1]);
        tf2.setVisible(true);
        tf2.setPromptText(columns[1][2]);
        tf3.setVisible(true);
        tf3.setPromptText(columns[1][3]);
        tf4.setVisible(true);
        tf4.setPromptText(columns[1][4]);
        tf5.setVisible(true);
        tf5.setPromptText(columns[1][5]);
        tf6.setVisible(true);
        tf6.setPromptText(columns[1][6]);
        tf7.setVisible(true);
        tf7.setPromptText(columns[1][7]);
        tf8.setVisible(true);
        tf8.setPromptText(columns[1][8]);
        tf9.setVisible(true);
        tf9.setPromptText(columns[1][9]);
        tf10.setVisible(true);
        tf10.setPromptText(columns[1][10]);
        tf11.setVisible(true);
        tf11.setPromptText(columns[1][11]);
        tf12.setVisible(false);
    }

    /**
     * Метод выбора таблицы личной информации студентов из базы данных для просмотра
     * @param actionEvent
     */
    public void dbStudentsData(ActionEvent actionEvent) {
        ScrollPane.setContent(TableViewCreate(2));
        saveButton.setVisible(true);
        error.setText("");
        studentsDataTF();
    }

    /**
     * Метод изменения внешности и заднего текста текстовых полей
     */
    private void studentsDataTF() {
        tf1.setVisible(true);
        tf1.setPromptText(columns[2][1]);
        tf2.setVisible(true);
        tf2.setPromptText(columns[2][2]);
        tf3.setVisible(true);
        tf3.setPromptText(columns[2][3]);
        tf4.setVisible(true);
        tf4.setPromptText(columns[2][4]);
        tf5.setVisible(true);
        tf5.setPromptText(columns[2][5]);
        tf6.setVisible(true);
        tf6.setPromptText(columns[2][6]);
        tf7.setVisible(true);
        tf7.setPromptText(columns[2][7]);
        tf8.setVisible(true);
        tf8.setPromptText(columns[2][8]);
        tf9.setVisible(true);
        tf9.setPromptText(columns[2][9]);
        tf10.setVisible(true);
        tf10.setPromptText(columns[2][10]);
        tf11.setVisible(true);
        tf11.setPromptText(columns[2][11]);
        tf12.setVisible(true);
        tf12.setPromptText(columns[2][12]);
    }

    /**
     * Метод создания и заполнения таблицы данных в интерфейсе
     * @param tableId - номер таблицы в массиве значений
     * @return - готовая таблица для отображения
     */
    private TableView TableViewCreate(int tableId) {
        TableView table = new TableView();
        tableChooseId = tableId;
        for (int i = 1; i < (columns[tableId]).length; i++) {
            TableColumn<StudentsInfo, String> column = new TableColumn<>((columns[tableId])[i]);
            column.setMinWidth(130);
            column.setCellValueFactory(new PropertyValueFactory<>(config.objectsValues[tableId][i - 1]));
            table.getColumns().add(column);
        }
        table.setItems(db.dbGetInfo(tableId));
        return table;
    }
}