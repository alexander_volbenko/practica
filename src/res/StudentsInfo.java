package res;

public class StudentsInfo {
    private String stId;
    private String fio;
    private String dateOfBirth;
    private String address;
    private String telephone;

    /**
     * Класс нужный для заполнения таблицы интерфейса
     * @param stId - Код студента
     * @param fio - ФИО
     * @param dateOfBirth - Дата рождения
     * @param address - Адрес
     * @param telephone - Телефон
     */
    public StudentsInfo(String stId, String fio, String dateOfBirth, String address, String telephone){
        this.stId = stId;
        this.fio = fio;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.telephone = telephone;
    }

    public String getStId() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
