package res;

public class ParentsInfo {
    private String stId;
    private String familyStatus;
    private String numberOfChildren;
    private String fiom;
    private String adressm;
    private String telephonem;
    private String jobPlacem;
    private String fiop;
    private String adressp;
    private String telephonep;
    private String jobPlacepp;

    /**
     * Метод нужный для отображения данных в таблице интерфейса
     * @param stId - Код студента
     * @param familyStatus - Статус семьи
     * @param numberOfChildren - Количество детей
     * @param fiom - ФИО матери
     * @param adressm - Адрес матери
     * @param telephonem - Телефон матери
     * @param jobPlacem - Место работы матери
     * @param fiop - ФИО отца
     * @param adressp - Адрес отца
     * @param telephonep - Телефон отца
     * @param jobPlacepp - Место работы отца
     */
    public ParentsInfo(String stId, String familyStatus, String numberOfChildren, String fiom, String adressm, String telephonem, String jobPlacem, String fiop, String adressp, String telephonep, String jobPlacepp) {
        this.stId = stId;
        this.familyStatus = familyStatus;
        this.numberOfChildren = numberOfChildren;
        this.fiom = fiom;
        this.adressm = adressm;
        this.telephonem = telephonem;
        this.jobPlacem = jobPlacem;
        this.fiop = fiop;
        this.adressp = adressp;
        this.telephonep = telephonep;
        this.jobPlacepp = jobPlacepp;
    }

    public String getStId() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public String getFiom() {
        return fiom;
    }

    public void setFiom(String fiom) {
        this.fiom = fiom;
    }

    public String getAdressm() {
        return adressm;
    }

    public void setAdressm(String adressm) {
        this.adressm = adressm;
    }

    public String getTelephonem() {
        return telephonem;
    }

    public void setTelephonem(String telephonem) {
        this.telephonem = telephonem;
    }

    public String getJobPlacem() {
        return jobPlacem;
    }

    public void setJobPlacem(String jobPlacem) {
        this.jobPlacem = jobPlacem;
    }

    public String getFiop() {
        return fiop;
    }

    public void setFiop(String fiop) {
        this.fiop = fiop;
    }

    public String getAdressp() {
        return adressp;
    }

    public void setAdressp(String adressp) {
        this.adressp = adressp;
    }

    public String getTelephonep() {
        return telephonep;
    }

    public void setTelephonep(String telephonep) {
        this.telephonep = telephonep;
    }

    public String getJobPlacepp() {
        return jobPlacepp;
    }

    public void setJobPlacepp(String jobPlacepp) {
        this.jobPlacepp = jobPlacepp;
    }
}
