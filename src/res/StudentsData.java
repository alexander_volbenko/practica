package res;

public class StudentsData {
    private String stId;
    private String fio;
    private String dateOfBirthPas;
    private String placeOfBirthPas;
    private String seriesAndNumberPas;
    private String issuesDatePas;
    private String issuedByPasandCodePas;
    private String inn;
    private String snils;
    private String seriesAndNumberBC;
    private String issuedByBC;
    private String issuesDateBC;

    /**
     * Класс нужный для заполнения таблицы данных интерфейса
     * @param stId - Код студента
     * @param fio - ФИО
     * @param dateOfBirthPas - Дата рождения
     * @param placeOfBirthPas - Место рождения
     * @param seriesAndNumberPas - Серия и номер паспорта
     * @param issuesDatePas - Дата получения
     * @param issuedByPasandCodePas - Кем выдан и код
     * @param inn - ИНН
     * @param snils - СНИЛС
     * @param seriesAndNumberBC - Серия и номер свидетельства о рождении
     * @param issuedByBC - Кем выдан
     * @param issuesDateBC - Дата выдачи
     */
    public StudentsData(String stId, String fio, String dateOfBirthPas, String placeOfBirthPas, String seriesAndNumberPas, String issuesDatePas, String issuedByPasandCodePas, String inn, String snils, String seriesAndNumberBC, String issuedByBC, String issuesDateBC) {
        this.stId = stId;
        this.fio = fio;
        this.dateOfBirthPas = dateOfBirthPas;
        this.placeOfBirthPas = placeOfBirthPas;
        this.seriesAndNumberPas = seriesAndNumberPas;
        this.issuesDatePas = issuesDatePas;
        this.issuedByPasandCodePas = issuedByPasandCodePas;
        this.inn = inn;
        this.snils = snils;
        this.seriesAndNumberBC = seriesAndNumberBC;
        this.issuedByBC = issuedByBC;
        this.issuesDateBC = issuesDateBC;
    }

    public String getStId() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getDateOfBirthPas() {
        return dateOfBirthPas;
    }

    public void setDateOfBirthPas(String dateOfBirthPas) {
        this.dateOfBirthPas = dateOfBirthPas;
    }

    public String getPlaceOfBirthPas() {
        return placeOfBirthPas;
    }

    public void setPlaceOfBirthPas(String placeOfBirthPas) {
        this.placeOfBirthPas = placeOfBirthPas;
    }

    public String getSeriesAndNumberPas() {
        return seriesAndNumberPas;
    }

    public void setSeriesAndNumberPas(String seriesAndNumberPas) {
        this.seriesAndNumberPas = seriesAndNumberPas;
    }

    public String getIssuesDatePas() {
        return issuesDatePas;
    }

    public void setIssuesDatePas(String issuesDatePas) {
        this.issuesDatePas = issuesDatePas;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getSeriesAndNumberBC() {
        return seriesAndNumberBC;
    }

    public void setSeriesAndNumberBC(String seriesAndNumberBC) {
        this.seriesAndNumberBC = seriesAndNumberBC;
    }

    public String getIssuedByBC() {
        return issuedByBC;
    }

    public void setIssuedByBC(String issuedByBC) {
        this.issuedByBC = issuedByBC;
    }

    public String getIssuesDateBC() {
        return issuesDateBC;
    }

    public void setIssuesDateBC(String issuesDateBC) {
        this.issuesDateBC = issuesDateBC;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getIssuedByPasandCodePas() {
        return issuedByPasandCodePas;
    }

    public void setIssuedByPasandCodePas(String issuedByPasandCodePas) {
        this.issuedByPasandCodePas = issuedByPasandCodePas;
    }
}
